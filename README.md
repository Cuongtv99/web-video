# web

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```
## Screenshots

> Delete the screenshot folder if you download this code (Screenshots folder is 3.14mb in size).

### Sign In (/login)

![Screenshot](Screenshot/Login.PNG)

### Sign Up (/login)

![Screenshot](Screenshot/Register.PNG)

### Home Page (/)

![Screenshot](Screenshots/)

### Subscriptions Page (/subscriptions)

![Screenshot](Screenshot/Subscription.PNG)

### History (Watch) Page (/history)

![Screenshot](Screenshot/History.PNG)

### Search Page (/search)

![Screenshot](Screenshot/Search.PNG)

### Search Page (/search)

![Screenshot](Screenshot/Search.PNG)

### Watch Page (/watch/:videoId)

![Screenshot](Screenshot/WatchingVideo.PNG)

### Comment & Reply (/watch/:videoId)

![Screenshot](Screenshot/WatchComments.PNG)

### Channel Page (/channels/Mychannel)

![Screenshot](Screenshot/MyChannel.PNG)

### Dashboard Page (/studio)

![Screenshot](Screenshot/Dashboard.PNG)

### Setting Modal (/studio)

![Screenshot](Screenshot/SettingInfo.PNG)

### Videos Page (/studio/videos)

![Screenshot](Screenshot/ManagerVideos.PNG)


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

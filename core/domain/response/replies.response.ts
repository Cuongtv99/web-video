import {SuccessResponse} from "./success.response";
import {Reply} from "../models/reply.model";

export class RepliesResponse extends SuccessResponse {
    count?: number;
    totalPage?: number;
    pagination?: object | any;
    results !: Reply[]
}

export class ReplyResponse extends SuccessResponse {
    results!: Reply
}
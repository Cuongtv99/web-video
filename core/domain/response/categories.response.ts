import {SuccessResponse} from "./success.response";
import {Category} from "../models/category.model";

export class CategoriesResponse extends SuccessResponse {
    results !: Category[]
}
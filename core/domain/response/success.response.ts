export class SuccessResponse {
    message!: string;
    success!: boolean;
}

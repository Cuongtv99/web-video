import {SuccessResponse} from "./success.response";
import {CommentsType} from "../models/comments.model";

export class CommentsResponse extends SuccessResponse {
    results !: CommentsType[]
}

export class CommentResponse extends SuccessResponse {
    results!: CommentsType
}
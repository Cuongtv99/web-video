import {SuccessResponse} from "./success.response";
import {Feeling} from "@core/domain/models/feeling.model";

export class FeelingResponse extends SuccessResponse {
    results!:  Feeling
}
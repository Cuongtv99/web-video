import {SuccessResponse} from "./success.response";
import {UserInfo} from "../models/user_info.model";

export class UserResponse extends SuccessResponse {
    results!: UserInfo
}

export class TokenResponse extends SuccessResponse {
    results!: string
}

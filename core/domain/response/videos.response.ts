import {VideoType} from "@core/domain/models/video.model";
import {SuccessResponse} from "@core/domain/response/success.response";

export class VideosResponse extends SuccessResponse {
    count?: number;
    totalPage?: number;
    pagination?: object | any;
    results!: VideoType[];
}

export class VideoResponse extends SuccessResponse {
    count?: number;
    totalPage?: number;
    pagination?: object | any;
    results!: VideoType
}

export class LoginRequest {
  email!: string;
  password!: string;
}

export class OAuthLoginRequest {
  provider!: string;
  token!: string;
}

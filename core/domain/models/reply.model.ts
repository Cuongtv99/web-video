import { UserInfo } from "@core/domain/models/user_info.model";

export class Reply {
  id!: string;
  text!: string;
  userId!: UserInfo;
  commentsId!: string;
  createdAt!: Date;
  updatedAt!: Date;

  constructor(
    id: string,
    text: string,
    userId: UserInfo,
    commentsId: string,
    createdAt: Date,
    updatedAt: Date
  ) {
    this.id = id;
    this.text = text;
    this.userId = userId;
    this.commentsId = commentsId;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }
}

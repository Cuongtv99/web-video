import { UserInfo } from "@core/domain/models/user_info.model";

export class VideoType {
  title!: string;
  description!: string;
  thumbnailUrl!: string;
  views!: number;
  status!: string;
  userId!: UserInfo;
  url!: string;
  createdAt!: Date;
  updatedAt!: Date;
  dislikes!: number;
  likes!: number;
  comments!: number;
  id!: string;

  constructor(
    title: string,
    description: string,
    thumbnailUrl: string,
    views: number,
    status: string,
    userId: UserInfo,
    url: string,
    createdAt: Date,
    updatedAt: Date,
    dislikes: number,
    likes: number,
    comments: number,
    id: string
  ) {
    this.title = title;
    this.description = description;
    this.thumbnailUrl = thumbnailUrl;
    this.views = views;
    this.status = status;
    this.userId = userId;
    this.url = url;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.dislikes = dislikes;
    this.likes = likes;
    this.comments = comments;
    this.id = id;
  }

  static fromObject(object: VideoType): VideoType {
    return new VideoType(
      object.title,
      object.description,
      object.thumbnailUrl,
      object.views,
      object.status,
      object.userId,
      object.url,
      object.createdAt,
      object.updatedAt,
      object.dislikes,
      object.likes,
      object.comments,
      object.id
    );
  }
}

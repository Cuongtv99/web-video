import {UserInfo} from "./user_info.model";
import {VideoType} from "@core/domain/models/video.model";

export class SubscriptionType {
    id!: string;
    subscriberId!: string;
    channelId!: UserInfo;
    createdAt!: Date;
    updatedAt!: Date;
    videos!: VideoType[]

    constructor(
        id: string,
        subscriberId: string,
        channelId: UserInfo,
        createAt: Date,
        updateAt: Date,
        videos: VideoType[]
    ) {
        this.id = id;
        this.subscriberId = subscriberId;
        this.channelId = channelId;
        this.createdAt = createAt;
        this.updatedAt = updateAt;
        this.videos = videos
    }
}

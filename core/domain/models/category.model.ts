export class Category {
  title?: string;
  descriptions?: string;
  userId!: number;
  createdAt?: Date;
  updatedAt?: Date;

  constructor(ob: any) {
    this.userId = ob.userId;
    this.title = ob.title;
    this.descriptions = ob.descriptions;
    this.userId = ob.userId;
    this.createdAt = ob.createdAt;
    this.updatedAt = ob.updatedAt;
  }
}

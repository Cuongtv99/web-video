export class ErrorResponse extends Error {
  constructor(
    message: string,
    public statusCode?: number,
    public reason?: string
  ) {
    super(message);
  }

  static fromObject(ex: ErrorResponse): ErrorResponse {
    return new ErrorResponse(ex.message, ex.statusCode, ex.reason);
  }
}

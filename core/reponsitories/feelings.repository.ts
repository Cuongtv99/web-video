import {FeelingResponse} from "@core/domain/response";
import { InjectValue } from "typescript-ioc";
import { DIKeys } from "@core/modules";
import { BaseClient } from "@core/services";

export abstract class FeelingsRepository {
  abstract changeFeeling(
    videoId: string,
    type: string
  ): Promise<FeelingResponse>;

  abstract checkFeeling(videoId: string): Promise<FeelingResponse>;

  abstract getLikedVideo(): Promise<FeelingResponse>;
}

export class HttpFeelingRepository implements FeelingsRepository {
  private apiPath = "/api/v1/feelings";
  @InjectValue(DIKeys.authClient)
  private httpClient!: BaseClient;

  changeFeeling(videoId: string, type: string): Promise<FeelingResponse> {
    return this.httpClient.post(`${this.apiPath}`, {
      videoId,
      type
    });
  }

  checkFeeling(videoId: string): Promise<FeelingResponse> {
    return this.httpClient.post(`${this.apiPath}/check`, {
      videoId
    });
  }

  getLikedVideo(): Promise<FeelingResponse> {
    return this.httpClient.get(`${this.apiPath}/videos`);
  }
}

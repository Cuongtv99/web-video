import {RepliesResponse, ReplyResponse} from "../domain/response";
import { InjectValue } from "typescript-ioc";
import { DIKeys } from "../modules";
import { BaseClient } from "../services";

export abstract class RepliesRepository {
  abstract getReply(): Promise<RepliesResponse>;

  abstract createReply(
    commentId: string,
    text: string
  ): Promise<ReplyResponse>;

  abstract updateReply(
    commentId: string,
    text: string
  ): Promise<ReplyResponse>;

  abstract deleteReply(commentId: string): Promise<ReplyResponse>;
}

export class HttpRepliesRepository implements RepliesRepository {
  private apiPath = "/api/v1/replies";
  @InjectValue(DIKeys.authClient)
  private httpClient!: BaseClient;

  getReply(): Promise<RepliesResponse> {
    return this.httpClient.get(`${this.apiPath}`);
  }

  createReply(commentId: string, text: string): Promise<ReplyResponse> {
    return this.httpClient.post(`${this.apiPath}`, {
      commentId,
      text
    });
  }

  updateReply(commentId: string, text?: string): Promise<ReplyResponse> {
    return this.httpClient.put(
      `${this.apiPath}/${commentId}`,
      {
        text
      }
    );
  }

  deleteReply(commentId: string): Promise<ReplyResponse> {
    return this.httpClient.delete(
      `${this.apiPath}/${commentId}`
    );
  }
}

import { BaseClient } from "../services";
import { InjectValue } from "typescript-ioc";
import { DIKeys } from "@core/modules";
import {CategoriesResponse} from "@core/domain/response/categories.response";

export abstract class CategoriesRepository {
  abstract getAllCategories(): Promise<CategoriesResponse>;
}

export class HttpCategoriesRepository implements CategoriesRepository {
  private apiPath = "/api/v1/categories";

  //limit - query

  @InjectValue(DIKeys.authClient)
  private httpClient!: BaseClient;

  getAllCategories(): Promise<CategoriesResponse> {
    return this.httpClient.get(`${this.apiPath}?limit=16`);
  }
}

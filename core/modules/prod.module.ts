import { DevModule } from "./dev.module";

class ProdModule extends DevModule {}

const prodModule = new DevModule();

export { prodModule, ProdModule };

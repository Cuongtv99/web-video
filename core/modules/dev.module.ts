import { BaseModule } from "@core/modules/module";
import { JsonUtils } from "@core/utils";
import { Container, Scope } from "typescript-ioc";
import { DI, DIKeys } from "@core/modules/di";
import {
  AuthenticationServiceImpl,
  BaseClient,
  CookieManger,
  CookieMangerImpl,
  DataManager,
  DataManagerImpl,
  HttpClient,
  UploadFileService,
  UploadFileServiceImpl,
  VideosServiceImpl
} from "@core/services";
import Axios, { AxiosError, AxiosRequestConfig } from "axios";
import {
  HttpAuthenticationRepository,
  HttpVideosRepository,
  VideosRepository
} from "@core/reponsitories";
import { ErrorHandler, ErrorResponse } from "@core/domain/exception";
import { StringUtils } from "@/shared/utils/string.utils";
import { ListUtils } from "@/shared/utils/list.utils";
import { AuthenticationModule } from "@/store/modules/authentication.store";
import {
  CategoriesRepository,
  HttpCategoriesRepository
} from "@core/reponsitories/categories.repository";
import {
  CategoriesService,
  CategoriesServiceImpl
} from "@core/services/categories.service";
import {
  CommentsRepository,
  HttpCommentsRepository
} from "@core/reponsitories/comments.repository";
import {
  CommentsService,
  CommentsServiceImpl
} from "@core/services/comments.service";
import {
  FeelingsRepository,
  HttpFeelingRepository
} from "@core/reponsitories/feelings.repository";
import {
  FeelingsService,
  FeelingsServiceImpl
} from "@core/services/feelings.service";
import {
  HttpRepliesRepository,
  RepliesRepository
} from "@core/reponsitories/replies.repository";
import {
  RepliesService,
  RepliesServiceImpl
} from "@core/services/replies.service";
import {
  HttpSubscriptionsRepository,
  SubscriptionsRepository
} from "@core/reponsitories/subscriptions.repository";
import {
  SubscriptionsService,
  SubscriptionsServiceImpl
} from "@core/services/subscriptions.service";
import {
  HistoriesRepository,
  HttpHistoriesRepository
} from "@core/reponsitories/histories.repository";
import {
  HistoriesService,
  HistoriesServiceImpl
} from "@core/services/histories.service";
import {
  HttpSearchRepository,
  SearchRepository
} from "@core/reponsitories/search.repository";
import {
  SearchService,
  SearchServiceImpl
} from "@core/services/search.service";

class DevModule extends BaseModule {
  configuration(): void {
    Container.bindName(DIKeys.apiHost).to(
      process.env.VUE_APP_API_HOST || "http://localhost:8000"
    );

    Container.bindName(DIKeys.noAuthClient).to(this.buildNoAuthClient());
    Container.bindName(DIKeys.authClient).to(this.buildAuthClient());
    Container.bindName(DIKeys.guest).to(this.buildGuestClient());

    Container.bindName(DIKeys.noAuthService).to(
      this.buildNoAuthenticationService()
    );
    Container.bindName(DIKeys.authService).to(
      this.buildAuthenticationService()
    );
    Container.bindName(DIKeys.guestService).to(this.buildGuestService());

    Container.bind(VideosRepository)
      .to(HttpVideosRepository)
      .scope(Scope.Singleton);

    Container.bindName(DIKeys.privateVideosService).to(
      this.buildPrivateVideosService()
    );
    Container.bindName(DIKeys.publicVideosService).to(
      this.buildPublicVideosService()
    );

    Container.bind(HistoriesRepository)
      .to(HttpHistoriesRepository)
      .scope(Scope.Singleton);
    Container.bind(HistoriesService)
      .to(HistoriesServiceImpl)
      .scope(Scope.Singleton);
    Container.bind(SearchRepository)
      .to(HttpSearchRepository)
      .scope(Scope.Singleton);
    Container.bind(SearchService)
      .to(SearchServiceImpl)
      .scope(Scope.Singleton);
    Container.bind(SubscriptionsRepository)
      .to(HttpSubscriptionsRepository)
      .scope(Scope.Singleton);
    Container.bind(SubscriptionsService)
      .to(SubscriptionsServiceImpl)
      .scope(Scope.Singleton);
    Container.bind(RepliesRepository)
      .to(HttpRepliesRepository)
      .scope(Scope.Singleton);
    Container.bind(RepliesService)
      .to(RepliesServiceImpl)
      .scope(Scope.Singleton);
    Container.bind(CategoriesRepository)
      .to(HttpCategoriesRepository)
      .scope(Scope.Singleton);
    Container.bind(CategoriesService)
      .to(CategoriesServiceImpl)
      .scope(Scope.Singleton);
    Container.bind(CommentsRepository)
      .to(HttpCommentsRepository)
      .scope(Scope.Singleton);
    Container.bind(CommentsService)
      .to(CommentsServiceImpl)
      .scope(Scope.Singleton);
    Container.bind(FeelingsRepository)
      .to(HttpFeelingRepository)
      .scope(Scope.Singleton);
    Container.bind(FeelingsService)
      .to(FeelingsServiceImpl)
      .scope(Scope.Singleton);
    Container.bind(UploadFileService)
      .to(UploadFileServiceImpl)
      .scope(Scope.Singleton);
    Container.bind(CookieManger)
      .to(CookieMangerImpl)
      .scope(Scope.Singleton);
    Container.bind(DataManager)
      .to(DataManagerImpl)
      .scope(Scope.Singleton);
  }

  buildNoAuthClient(): BaseClient {
    const apiHost = Container.getValue(DIKeys.apiHost);
    const timeOut = process.env.VUE_APP_TIME_OUT || 30000;
    const client = Axios.create({
      baseURL: apiHost,
      timeout: timeOut,
      headers: {
        "Content-Type": "application/json"
      },

      transformRequest: data => {
        if (data instanceof FormData) {
          return data;
        } else {
          return JsonUtils.toCamelKey(data);
        }
      },
      withCredentials: false
    });
    console.log(
      "DevModule::",
      "buildNoAuthClient:: with API HOST:",
      apiHost,
      "Time out",
      timeOut
    );
    return new HttpClient(client);
  }

  buildNoAuthenticationService() {
    const noAuthClient = DI.get<BaseClient>(DIKeys.noAuthClient);
    const authenticationRepository = new HttpAuthenticationRepository(
      noAuthClient
    );
    return new AuthenticationServiceImpl(authenticationRepository);
  }

  buildAuthClient(): BaseClient {
    const apiHost = Container.getValue(DIKeys.apiHost);
    const client = Axios.create({
      baseURL: apiHost,
      timeout: process.env.VUE_APP_TIME_OUT || 30000,
      headers: {
        "Content-Type": "application/json"
      },
      transformRequest: data => {
        if (data instanceof FormData) {
          return data;
        } else {
          return JsonUtils.toCamelKey(data);
        }
      },
      withCredentials: false
    });

    client.interceptors.request.use(
      value => this.injectTokenAndSession(value),
      error => {
        return Promise.reject(error);
      }
    );
    client.interceptors.response.use(
      value => {
        return value;
      },
      (...error) => this.responseError(error)
    );
    console.log("DevModule::", "buildAuthClient:: with API HOST:", apiHost);
    return new HttpClient(client);
  }

  buildAuthenticationService() {
    const authClient = DI.get<BaseClient>(DIKeys.authClient);
    const authenticationRepository = new HttpAuthenticationRepository(
      authClient
    );
    return new AuthenticationServiceImpl(authenticationRepository);
  }

  buildGuestClient(): BaseClient {
    const apiHost = Container.getValue(DIKeys.apiHost);
    const client = Axios.create({
      baseURL: apiHost,
      timeout: process.env.VUE_APP_TIME_OUT || 30000,
      headers: {
        "Content-Type": "application/json"
      },
      transformRequest: data => {
        if (data instanceof FormData) {
          return data;
        } else {
          return JsonUtils.toJson(data);
        }
      },
      withCredentials: false
    });

    client.interceptors.request.use(
      value => this.injectSession(value),
      error => {
        return Promise.reject(error);
      }
    );
    client.interceptors.response.use(
      value => {
        return value;
      },
      (...error) => this.responseError(error)
    );
    console.log("DevModule::", "buildGuestClient:: with API HOST:", apiHost);
    return new HttpClient(client);
  }

  buildGuestService() {
    const guest = DI.get<BaseClient>(DIKeys.guest);
    const authenticationRepository = new HttpAuthenticationRepository(guest);
    return new AuthenticationServiceImpl(authenticationRepository);
  }

  buildPrivateVideosService() {
    const privateVideos = DI.get<BaseClient>(DIKeys.authClient);
    const videosRepository = new HttpVideosRepository(privateVideos);
    return new VideosServiceImpl(videosRepository);
  }

  buildPublicVideosService() {
    const publicVideos = DI.get<BaseClient>(DIKeys.noAuthClient);
    const videosRepository = new HttpVideosRepository(publicVideos);
    return new VideosServiceImpl(videosRepository);
  }

  private injectTokenAndSession(
    request: AxiosRequestConfig
  ): AxiosRequestConfig {
    const dataManager = DI.get(DataManager);
    const session = dataManager.getSession();
    const token = dataManager.getToken();
    if (token) {
      request.headers["Token-Id"] = token;
    }
    if (session && StringUtils.isNotEmpty(session)) {
      request.headers["Authorization"] = `Bearer ${session}`;
    }
    return request;
  }

  private injectSession(request: AxiosRequestConfig): AxiosRequestConfig {
    const dataManager = DI.get(DataManager);
    const session = dataManager.getSession();
    if (session && StringUtils.isNotEmpty(session)) {
      request.headers["Authorization"] = session;
    }
    return request;
  }

  responseError(errors: any[]): Promise<ErrorResponse> {
    if (ListUtils.isNotEmpty(errors)) {
      const apiException = DevModule.parseError(errors[0]);
      if (apiException.reason === "notAuthenticated") {
        AuthenticationModule.signOut();
      }
      return Promise.reject(apiException);
    } else {
      return Promise.reject(new ErrorResponse("Unknown exception"));
    }
  }

  private static parseError(reason: AxiosError<string>): ErrorResponse {
    if (reason.response?.data) {
      const apiException = JsonUtils.fromJson<any>(reason.response.data);
      return ErrorHandler.fromObject(apiException);
    } else {
      return new ErrorResponse(reason.request);
    }
  }
}

class TestModule extends DevModule {
  configuration(): void {
    Container.bindName(DIKeys.apiHost).to("http://localhost:8000");

    Container.bindName(DIKeys.noAuthClient).to(this.buildNoAuthClient());
    Container.bindName(DIKeys.authClient).to(this.buildNoAuthClient());

    Container.bindName(DIKeys.noAuthService).to(
      this.buildNoAuthenticationService()
    );
    Container.bindName(DIKeys.authService).to(
      this.buildNoAuthenticationService()
    );
    Container.bind(CategoriesRepository)
      .to(HttpCategoriesRepository)
      .scope(Scope.Singleton);
    Container.bind(CategoriesService)
      .to(CategoriesServiceImpl)
      .scope(Scope.Singleton);
    Container.bind(CommentsRepository)
      .to(HttpCommentsRepository)
      .scope(Scope.Singleton);
    Container.bind(CommentsService)
      .to(CommentsServiceImpl)
      .scope(Scope.Singleton);
    Container.bind(FeelingsRepository)
      .to(HttpFeelingRepository)
      .scope(Scope.Singleton);
    Container.bind(FeelingsService)
      .to(FeelingsServiceImpl)
      .scope(Scope.Singleton);
    Container.bind(UploadFileService)
      .to(UploadFileServiceImpl)
      .scope(Scope.Singleton);
    Container.bind(CookieManger)
      .to(CookieMangerImpl)
      .scope(Scope.Singleton);
    Container.bind(DataManager)
      .to(DataManagerImpl)
      .scope(Scope.Singleton);
  }
}

const testModule = new TestModule();
const devModule = new DevModule();

export { devModule, testModule, DevModule };

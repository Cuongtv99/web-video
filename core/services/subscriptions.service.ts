import {Inject} from "typescript-ioc";
import {SubscriptionsRepository} from "../reponsitories/subscriptions.repository";
import {SubscriptionResponse, SubscriptionsResponse} from "@core/domain/response/supscription.response";

export abstract class SubscriptionsService {
    abstract channels(subscriberId: string): Promise<SubscriptionsResponse>;

    abstract subscribers(): Promise<SubscriptionsResponse>

    abstract subscribe(channelId: string): Promise<SubscriptionResponse>;

    abstract checkSubscription(channelId: string): Promise<SubscriptionResponse>;
}

export class SubscriptionsServiceImpl implements SubscriptionsService {
    constructor(@Inject private repository: SubscriptionsRepository) {
    }

    checkSubscription(channelId: string): Promise<SubscriptionResponse> {
        return this.repository.checkSubscription(channelId);
    }

    subscribe(channelId: string): Promise<SubscriptionResponse> {
        return this.repository.subscribe(channelId);
    }

    channels(subscriberId: string): Promise<SubscriptionsResponse> {
        return this.repository.channels(subscriberId);

    }

    subscribers(): Promise<SubscriptionsResponse> {
        return this.repository.subscribers();
    }

}

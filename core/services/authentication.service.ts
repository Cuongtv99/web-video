import { RegisterRequest } from "../domain/request/register.request";
import {
  LoginRequest,
  OAuthLoginRequest
} from "../domain/request/login.request";
import { AuthenticationRepository } from "../reponsitories/authentication.repository";
import {TokenResponse, UserResponse} from "@core/domain/response/user.response";

export abstract class AuthenticationService {
  abstract register(request: RegisterRequest): Promise<UserResponse>;

  abstract login(request: LoginRequest): Promise<TokenResponse>;

  abstract getUserLogged(): Promise<UserResponse>;

  abstract oauthLogin(request: OAuthLoginRequest): Promise<TokenResponse>;

  abstract updateDetails(
    email: string,
    channelName: string
  ): Promise<UserResponse>;

  abstract updateAvatar(url: string): Promise<UserResponse>;

  abstract updatePassword(
    currentPassword: string,
    newPassword: string
  ): Promise<UserResponse>;

  abstract forgotPassword(email: string): Promise<UserResponse>;

  abstract resetPassword(
    token: string,
    newPassword: string
  ): Promise<UserResponse>;

  abstract getUser(id: string): Promise<UserResponse>;

  abstract logout(): Promise<UserResponse>;
}

export class AuthenticationServiceImpl implements AuthenticationService {
  constructor(private repository: AuthenticationRepository) {}

  register(request: RegisterRequest): Promise<UserResponse> {
    return this.repository.register(request);
  }

  login(request: LoginRequest): Promise<TokenResponse> {
    return this.repository.login(request);
  }

  getUserLogged(): Promise<UserResponse> {
    return this.repository.getUserLogged();
  }

  oauthLogin(request: OAuthLoginRequest): Promise<TokenResponse> {
    return this.repository.oauthLogin(request);
  }

  updateDetails(email: string, channelName: string): Promise<UserResponse> {
    return this.repository.updateDetails(email, channelName);
  }

  updateAvatar(url: string): Promise<UserResponse> {
    return this.repository.updateAvatar(url);
  }

  updatePassword(
    currentPassword: string,
    newPassword: string
  ): Promise<UserResponse> {
    return this.repository.updatePassword(currentPassword, newPassword);
  }

  forgotPassword(email: string): Promise<UserResponse> {
    return this.repository.forgotPassword(email);
  }

  resetPassword(token: string, newPassword: string): Promise<UserResponse> {
    return this.repository.resetPassword(token, newPassword);
  }

  getUser(id: string): Promise<UserResponse> {
    return this.repository.getUser(id);
  }

  logout(): Promise<UserResponse> {
    return this.repository.logout();
  }
}

import firebase from "firebase";
import "firebase/app";
import { randomString } from "@/shared/utils/randomStringsForTest.utils";

export abstract class UploadFileService {
  abstract uploadImage(file: File, userId: string): Promise<string>;

  abstract uploadVideo(file: File, userId: string): Promise<string>;

  abstract uploadThumbnail(file: File, userId: string): Promise<string>;

  abstract deleteThumbnail(nameFile: string, userId: string): Promise<boolean>;

  abstract deleteVideo(nameFile: string, userId: string): Promise<boolean>;

  abstract deleteImage(file: string, userId: string): Promise<boolean>;
}

export class UploadFileServiceImpl extends UploadFileService {
  private apiPath = "firebase/";
  storageRef = firebase.storage().ref(`${this.apiPath}`);

  uploadImage(file: File, userId: string): Promise<string> {
    const metadata = {
      contentType: "image/jpeg"
    };

    const childRef = this.storageRef.child(
      `images/${userId}/avatar/${file.name}${randomString()}`
    );
    return childRef.put(file, metadata).then(snapshot => {
      return new Promise<boolean>(resolve => {
        snapshot.task.on(
          firebase.storage.TaskEvent.STATE_CHANGED,
          function(snapshots) {
            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            const progress =
              (snapshots.bytesTransferred / snapshots.totalBytes) * 100;
            console.log("Upload is " + progress + "% done");
            switch (snapshots.state) {
              case firebase.storage.TaskState.PAUSED: // or 'paused'
                console.log("Upload is paused");
                break;
              case firebase.storage.TaskState.RUNNING: // or 'running'
                console.log("Upload is running");
                break;
            }
          },
          function() {
            //
            //
            //
          },
          function() {
            snapshot.ref.getDownloadURL().then(function(downloadURL) {
              return resolve(downloadURL);
            });
          }
        );
      });
    });
  }

  uploadVideo(file: File, userId: string): Promise<string> {
    const metadata = {
      contentType: "video/mp4"
    };

    const childRef = this.storageRef.child(
      `videos/${userId}/video/${file.name}${randomString()}`
    );
    return childRef.put(file, metadata).then(snapshot => {
      return new Promise<string>(resolve => {
        snapshot.task.on(
          firebase.storage.TaskEvent.STATE_CHANGED,
          function(snapshots) {
            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            const progress =
              (snapshots.bytesTransferred / snapshots.totalBytes) * 100;
            console.log("Upload is " + progress + "% done");
            switch (snapshots.state) {
              case firebase.storage.TaskState.PAUSED: // or 'paused'
                console.log("Upload is paused");
                break;
              case firebase.storage.TaskState.RUNNING: // or 'running'
                console.log("Upload is running");
                break;
            }
          },
          function() {
            //
            //
            //
            return resolve("Cannot upload to firebase");
          },
          function() {
            snapshot.ref.getDownloadURL().then(function(downloadURL) {
              return resolve(downloadURL);
            });
          }
        );
      });
    });
  }

  uploadThumbnail(file: File, userId: string): Promise<string> {
    const metadata = {
      contentType: "image/jpeg"
    };

    const childRef = this.storageRef.child(
      `videos/${userId}/thumbnail/${file.name}${randomString()}`
    );
    return childRef.put(file, metadata).then(snapshot => {
      return new Promise<string>(resolve => {
        snapshot.task.on(
          firebase.storage.TaskEvent.STATE_CHANGED,
          function(snapshots) {
            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            const progress =
              (snapshots.bytesTransferred / snapshots.totalBytes) * 100;
            console.log("Upload is " + progress + "% done");
            switch (snapshots.state) {
              case firebase.storage.TaskState.PAUSED: // or 'paused'
                console.log("Upload is paused");
                break;
              case firebase.storage.TaskState.RUNNING: // or 'running'
                console.log("Upload is running");
                break;
            }
          },
          function() {
            //
            //
            //
            return resolve("Cannot upload to firebase");
          },
          function() {
            snapshot.ref.getDownloadURL().then(function(downloadURL) {
              console.log("Upload thumbnail");
              return resolve(downloadURL);
            });
          }
        );
      });
    });
  }

  deleteThumbnail(nameFile: string, userId: string): Promise<boolean> {
    const childRef = this.storageRef.child(`videos/${userId}/thumbnail`);
    // Delete the file
    return childRef.listAll().then(
      value => {
        value.items.forEach(fileRef => {
          if (nameFile === fileRef.name) {
            this.deleteFile(childRef.fullPath, fileRef.name);
          }
        });
        return new Promise(resolve => resolve(true));
      },
      _ => new Promise(resolve => resolve(false))
    );
  }

  deleteVideo(nameFile: string, userId: string): Promise<boolean> {
    const childRef = this.storageRef.child(`videos/${userId}/video`);
    // Delete the file
    return childRef.listAll().then(
      value => {
        value.items.forEach(fileRef => {
          if (nameFile === fileRef.name) {
            this.deleteFile(childRef.fullPath, fileRef.name);
          }
        });
        return new Promise(resolve => resolve(true));
      },
      _ => new Promise(resolve => resolve(false))
    );
  }

  // delete file on store cloud
  deleteFile(pathToFile: string, fileName: string): Promise<boolean> {
    const ref = firebase.storage().ref(pathToFile);
    const childRef = ref.child(fileName);
    return childRef.delete().then(
      function() {
        console.log("deleteFile:: success");
        return new Promise(resolve => resolve(true));
      },
      function() {
        console.log("deleteFile:: error");
        return new Promise(resolve => resolve(false));
      }
    );
  }

  deleteImage(file: string, userId: string): Promise<boolean> {
    const childRef = this.storageRef.child(`images/${userId}/avatar`);

    return childRef.listAll().then(
      value => {
        value.items.find(fileRef => {
          this.deleteFile(childRef.fullPath, fileRef.name);
        });
        return new Promise(resolve => resolve(true));
      },
      _ => new Promise(resolve => resolve(false))
    );
  }
}

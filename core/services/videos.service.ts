import {VideosRequest} from "@core/domain/request";
import {Inject} from "typescript-ioc";
import {VideosRepository} from "@core/reponsitories";
import {VideoResponse, VideosResponse} from "@core/domain/response/videos.response";

export abstract class VideosService {
    abstract getAllPublicVideos(): Promise<VideosResponse>;

    abstract getAllPrivateVideos(): Promise<VideosResponse>;

    abstract uploadVideo(request: VideosRequest): Promise<VideosResponse>;

    abstract getVideo(videoId: string): Promise<VideoResponse>;

    abstract updateVideo(
        videoId: string,
        status: string,
        userId: string,
        thumbnailUrl: string,
        title: string,
        description: string,
        category: string
    ): Promise<VideosResponse>;

    abstract deleteVideo(videoId: string): Promise<VideoResponse>;

    abstract updateViews(videoId: string): Promise<VideoResponse>;

    abstract updateThumbnail(videoId: string): Promise<VideosResponse>;

    abstract latestVideo(): Promise<VideoResponse>;
}

export class VideosServiceImpl implements VideosService {
    constructor(@Inject private repository: VideosRepository) {
    }

    getAllPublicVideos(): Promise<VideosResponse> {
        return this.repository.getAllPublicVideos();
    }

    getAllPrivateVideos(): Promise<VideosResponse> {
        return this.repository.getAllPrivateVideos();
    }

    uploadVideo(request: VideosRequest): Promise<VideosResponse> {
        return this.repository.uploadVideo(request);
    }

    deleteVideo(videoId: string): Promise<VideoResponse> {
        return this.repository.deleteVideo(videoId);
    }

    getVideo(videoId: string): Promise<VideoResponse> {
        return this.repository.getVideo(videoId);
    }

    updateVideo(
        videoId: string,
        status: string,
        userId: string,
        thumbnailUrl: string,
        title: string,
        description: string,
        category: string
    ): Promise<VideosResponse> {
        return this.repository.updateVideo(
            videoId,
            status,
            userId,
            thumbnailUrl,
            title,
            description,
            category
        );
    }

    updateViews(videoId: string): Promise<VideoResponse> {
        return this.repository.updateViews(videoId);
    }

    updateThumbnail(videoId: string): Promise<VideosResponse> {
        return this.repository.updateThumbnail(videoId);
    }

    latestVideo(): Promise<VideoResponse> {
        return this.repository.latestVideo();
    }
}

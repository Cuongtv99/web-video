import { CookieManger } from "@core/services";
import { JsonUtils } from "@core/utils";
import { Inject } from "typescript-ioc";
import { UserInfo } from "@core/domain/models/user_info.model";
import { VideoType } from "@core/domain/models/video.model";
import { Transform } from "@/shared/utils/transform.utils";
import { VideoModel } from "@/shared/models/video.model";
import { SubscriptionType } from "@core/domain/models/subscription.model";

enum DataManagerKeys {
  userProfile = "user_profile",
  sessionId = "ssid",
  token = "token",
  privateVideo = "private_video",
  publicVideo = "public_video",
  video = "video",
  subscribers = "subscribers",
  videosSubscriber = "videosSubscriber"
}

export abstract class DataManager {
  abstract saveSession(session: string): boolean;

  abstract getSession(): string | undefined;

  abstract saveUserProfile(profile: UserInfo): boolean;

  abstract getUserProfile(): UserInfo;

  abstract clearUserData(): boolean;

  abstract getToken(): string | null;

  abstract setToken(token: string): boolean;

  abstract getPrivateVideos(): VideoModel[];

  abstract savePrivateVideos(video: VideoType[]): boolean;

  abstract getPublicVideos(): VideoModel[];

  abstract savePublicVideos(video: VideoType[]): boolean;

  abstract saveVideo(video: VideoType): boolean;

  abstract getVideo(): VideoModel;

  abstract clearVideo(): boolean;

  abstract removeToken(): boolean;

  abstract getSubscribers(): SubscriptionType[];

  abstract saveSubscribers(user: SubscriptionType[]): boolean;

  abstract clearSubscribers(): boolean;

  abstract getVideosSubscriber(): VideoModel[];

  abstract saveVideosSubscriber(video: VideoType[]): boolean;

  abstract clearVideosSubscriber(): boolean;
}

export class DataManagerImpl extends DataManager {
  @Inject
  private cookieManger!: CookieManger;

  saveSession(session: string): boolean {
    return this.cookieManger.put(DataManagerKeys.sessionId, session);
  }

  getSession(): string | undefined {
    return this.cookieManger.get(DataManagerKeys.sessionId);
  }

  saveUserProfile(profile: UserInfo): boolean {
    localStorage.setItem(
      DataManagerKeys.userProfile,
      JsonUtils.toJson(profile)
    );
    return true;
  }

  getUserProfile(): UserInfo | any {
    const rawUserProfile = localStorage.getItem(DataManagerKeys.userProfile);
    if (rawUserProfile) return JsonUtils.fromJson(rawUserProfile);
    else return void 0;
  }

  clearUserData(): boolean {
    this.cookieManger.clear();
    localStorage.removeItem(DataManagerKeys.userProfile);
    return true;
  }

  getToken(): string | null {
    return sessionStorage.getItem(DataManagerKeys.token);
  }

  setToken(token: string): boolean {
    sessionStorage.setItem(DataManagerKeys.token, token);
    return true;
  }

  removeToken(): boolean {
    sessionStorage.removeItem(DataManagerKeys.token);
    return true;
  }

  getPrivateVideos(): VideoModel[] | any {
    const raw = localStorage.getItem(DataManagerKeys.privateVideo);
    if (raw) return JsonUtils.fromJson(raw);
    else return void 0;
  }

  savePrivateVideos(video: VideoType[]): boolean {
    let videos: VideoModel[] = [];
    video.map(value => {
      if (value) {
        videos = [...videos, Transform.toVideoModel(value)];
      }
    });
    localStorage.setItem(
      DataManagerKeys.privateVideo,
      JsonUtils.toCamelKey(videos)
    );
    return true;
  }

  getPublicVideos(): VideoModel[] | any {
    const raw = localStorage.getItem(DataManagerKeys.publicVideo);
    if (raw) return JsonUtils.fromJson(raw);
    else return void 0;
  }

  savePublicVideos(video: VideoType[]): boolean {
    let videos: VideoModel[] = [];
    video.map(value => {
      if (value) {
        videos = [...videos, Transform.toVideoModel(value)];
      }
    });
    localStorage.setItem(
      DataManagerKeys.publicVideo,
      JsonUtils.toCamelKey(videos)
    );
    return true;
  }

  getVideo(): VideoModel | any {
    const raw = localStorage.getItem(DataManagerKeys.video);
    if (raw) return JsonUtils.fromJson(raw);
    else return void 0;
  }

  saveVideo(video: VideoType): boolean {
    const formatData = Transform.toVideoModel(video);
    localStorage.setItem(DataManagerKeys.video, JsonUtils.toJson(formatData));
    return true;
  }

  clearVideo(): boolean {
    localStorage.removeItem(DataManagerKeys.publicVideo);
    localStorage.removeItem(DataManagerKeys.privateVideo);
    localStorage.removeItem(DataManagerKeys.video);
    localStorage.removeItem(DataManagerKeys.videosSubscriber);
    return true;
  }

  getSubscribers(): SubscriptionType[] | any {
    const raw = localStorage.getItem(DataManagerKeys.subscribers);
    if (raw) return JsonUtils.fromJson(raw);
    else return void 0;
  }

  saveSubscribers(user: SubscriptionType[]): boolean {
    localStorage.setItem(DataManagerKeys.subscribers, JsonUtils.toJson(user));
    return true;
  }

  clearSubscribers(): boolean {
    localStorage.removeItem(DataManagerKeys.subscribers);
    return true;
  }

  clearVideosSubscriber(): boolean {
    localStorage.removeItem(DataManagerKeys.videosSubscriber);
    return true;
  }

  getVideosSubscriber(): VideoModel[] | any {
    const raw = localStorage.getItem(DataManagerKeys.videosSubscriber);
    if (raw) return JsonUtils.fromJson(raw);
    else return void 0;
  }

  saveVideosSubscriber(video: VideoType[]): boolean {
    let videos: VideoModel[] = [];
    video.map(value => {
      if (value) {
        videos = [...videos, Transform.toVideoModel(value)];
      }
    });
    localStorage.setItem(
      DataManagerKeys.videosSubscriber,
      JsonUtils.toCamelKey(videos)
    );
    return true;
  }
}

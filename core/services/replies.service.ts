import {RepliesResponse, ReplyResponse} from "../domain/response";
import { Inject } from "typescript-ioc";
import { RepliesRepository } from "../reponsitories/replies.repository";

export abstract class RepliesService {
  abstract getReply(): Promise<RepliesResponse>;

  abstract createReply(
    commentId: string,
    text: string
  ): Promise<ReplyResponse>;

  abstract updateReply(
    commentId: string,
    text: string
  ): Promise<ReplyResponse>;

  abstract deleteReply(commentId: string): Promise<ReplyResponse>;
}

export class RepliesServiceImpl implements RepliesService {
  constructor(@Inject private repository: RepliesRepository) {}

  getReply(): Promise<RepliesResponse> {
    return this.repository.getReply();
  }

  deleteReply(commentId: string): Promise<ReplyResponse> {
    return this.repository.deleteReply(commentId);
  }

  createReply(commentId: string, text: string): Promise<ReplyResponse> {
    return this.repository.createReply(commentId, text);
  }

  updateReply(commentId: string, text: string): Promise<ReplyResponse> {
    return this.repository.updateReply(commentId, text);
  }
}

import { Inject } from "typescript-ioc";
import { SearchRepository } from "../reponsitories/search.repository";
import {SearchesResponse, SearchResponse} from "@core/domain/response/search.response";

export abstract class SearchService {
  abstract search(text: string): Promise<SearchResponse>;
}

export class SearchServiceImpl implements SearchService {
  constructor(@Inject private repository: SearchRepository) {}

  search(text: string): Promise<SearchResponse>{
    return this.repository.search(text);
  }
}

// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require("path");
module.exports = {
  transpileDependencies: ["vuetify", "vuex-module-decorators", "vuex-persist"],
  configureWebpack: {
    resolve: {
      alias: {
        "@": path.resolve(__dirname, "src"),
        "@core": path.resolve(__dirname, "core")
      }
    }
  }
};

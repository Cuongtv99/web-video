import {
  Action,
  getModule,
  Module,
  Mutation,
  VuexModule
} from "vuex-module-decorators";

import { Category } from "@core/domain/models/category.model";
import { Inject } from "typescript-ioc";
import { CategoriesService } from "@core/services/categories.service";
import store from "@/store";
import { Stores } from "@/shared/enums";

export interface CategoriesState {
  categories: Category[];
}

export interface LooseObject {
  [key: string]: any;
}

@Module({
  store: store,
  name: Stores.categoriesStore,
  dynamic: true,
  namespaced: true
})
export class CategoryStore extends VuexModule {
  categories: CategoriesState["categories"] = [];
  populatedCategories: LooseObject = {};
  @Inject
  categoriesService!: CategoriesService;

  @Mutation
  setCategories(categories: Category[]) {
    this.categories = categories;
  }

  get getPopulatedCategories() {
    return this.populatedCategories;
  }

  @Mutation
  private jsonToObject(categoriesList: object[]) {
    categoriesList.map((value: any) => {
      const { id, title } = value;
      if (id) {
        this.populatedCategories[`${title}`] = `${id}`;
      }
    });
  }

  @Action
  async getAllCategories() {
    try {
      const response = await this.categoriesService.getAllCategories();
      if (response.success) {
        this.setCategories(response.results);
        this.jsonToObject(this.categories);
      }
    } catch (e) {
      console.log("Cannot get all categories ::", e);
    }
  }
}

export const CategoriesModule = getModule(CategoryStore);

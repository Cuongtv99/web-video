import store from "@/store";
import {
  getModule,
  Module,
  Mutation,
  Action,
  VuexModule
} from "vuex-module-decorators";
import { VideoType } from "@core/domain/models/video.model";
import { Inject, InjectValue } from "typescript-ioc";
import { DIKeys } from "@core/modules";
import { DataManager, VideosService } from "@core/services";
import { VideosRequest } from "@core/domain/request";
import { Stores } from "@/shared/enums";
import { UserInfo } from "@core/domain/models/user_info.model";
import { VideoModel } from "@/shared/models/video.model";
import { Transform } from "@/shared/utils/transform.utils";
import { ListUtils } from "@/shared/utils/list.utils";

export interface VideoState {
  videos: VideoType[];
  video: VideoType;
  videoStatus: VideoStatus;
  latestVideo: VideoType;
}

export enum VideoStatus {
  undefined,
  loading,
  loaded
}

@Module({
  store: store,
  name: Stores.videosStore,
  dynamic: true,
  namespaced: true
})
export class VideoStore extends VuexModule {
  videoStatus: VideoState["videoStatus"] = VideoStatus.undefined;
  private privateVideos: VideoState["videos"] = [];
  private publicVideos: VideoState["videos"] = [];
  private video: VideoState["video"] = new VideoType(
    "",
    "",
    "",
    0,
    "",
    new UserInfo("", "", "", "", new Date(), new Date(), 0, ""),
    "",
    new Date(),
    new Date(),
    0,
    0,
    0,
    ""
  );
  private _latestVideo: VideoState["latestVideo"] = new VideoType(
    "",
    "",
    "",
    0,
    "",
    new UserInfo("", "", "", "", new Date(), new Date(), 0, ""),
    "",
    new Date(),
    new Date(),
    0,
    0,
    0,
    ""
  );
  @InjectValue(DIKeys.privateVideosService)
  privateVideosService!: VideosService;
  @InjectValue(DIKeys.publicVideosService)
  publicVideoService!: VideosService;
  @Inject
  dataManager!: DataManager;

  get getVideoStatus() {
    return this.videoStatus;
  }

  get getPrivateVideos(): VideoModel[] {
    if (ListUtils.isEmpty(this.privateVideos)) {
      return this.dataManager.getPrivateVideos();
    }
    let videos: VideoModel[] = [];
    this.privateVideos.map(value => {
      if (value) {
        videos = [...videos, Transform.toVideoModel(value)];
      }
    });
    return videos;
  }

  get getPublicVideos(): VideoModel[] {
    if (ListUtils.isEmpty(this.publicVideos)) {
      return this.dataManager.getPublicVideos();
    }
    let videos: VideoModel[] = [];
    this.publicVideos.map(value => {
      if (value) {
        videos = [...videos, Transform.toVideoModel(value)];
      }
    });
    return videos;
  }

  get getVideo(): VideoModel {
    if (this.video.id) {
      return this.dataManager.getVideo();
    }
    const video: VideoModel = Transform.toVideoModel(this.video);
    return video;
  }

  get getLatestVideo(): VideoModel {
    const video: VideoModel = Transform.toLatestVideoModel(this._latestVideo);
    return video;
  }

  @Mutation
  private setLatestVideo(video: VideoType): void {
    this._latestVideo = video;
  }

  @Mutation
  private setVideoStatus(state: VideoStatus): void {
    this.videoStatus = state;
  }

  @Mutation
  private savePrivateVideos(videos: VideoType[]) {
    this.privateVideos = [...videos];
  }

  @Mutation
  private savePublicVideos(videos: VideoType[]) {
    this.publicVideos = [...videos];
  }

  @Mutation
  private saveVideo(video: VideoType) {
    this.video = video;
  }

  @Action
  async latestVideo() {
    try {
      const response = await this.privateVideosService.latestVideo();
      if (response.results) {
        this.setLatestVideo(response.results);
      }
    } catch (e) {
      console.log("cannot get latest video::", e);
    }
  }

  @Action
  async deleteVideo(videoId: string) {
    try {
      this.setVideoStatus(VideoStatus.loading);
      const response = await this.privateVideosService.deleteVideo(videoId);
      this.removeVideo(response.results);
      this.setVideoStatus(VideoStatus.loaded);
    } catch (e) {
      console.log("Cannot delete video", e);
      this.setVideoStatus(VideoStatus.undefined);
    }
  }

  @Mutation
  removeVideo(video: VideoType) {
    this.privateVideos = this.privateVideos.filter(
      item => video.id !== item.id
    );
  }

  @Action
  async uploadVideo(payload: VideosRequest) {
    try {
      this.setVideoStatus(VideoStatus.loading);
      await this.privateVideosService.uploadVideo(payload);
      this.setVideoStatus(VideoStatus.loaded);
    } catch (e) {
      console.log("Cannot upload video", e);
      this.setVideoStatus(VideoStatus.undefined);
    }
  }

  // own videos
  @Action
  async getAndSavePrivateVideos() {
    try {
      this.setVideoStatus(VideoStatus.loading);
      const response = await this.privateVideosService.getAllPrivateVideos();
      if (response) {
        this.savePrivateVideos(response.results);
        this.dataManager.savePrivateVideos(response.results);
      }
      this.setVideoStatus(VideoStatus.loaded);
    } catch (e) {
      console.log("Cannot getPrivateVideos::", e);
      this.setVideoStatus(VideoStatus.undefined);
    }
  }

  @Action
  async getAndSavePublicVideos() {
    try {
      this.setVideoStatus(VideoStatus.loading);
      const response = await this.publicVideoService.getAllPublicVideos();
      if (response) {
        this.savePublicVideos(response.results);
        this.dataManager.savePublicVideos(response.results);
      }
      this.setVideoStatus(VideoStatus.loaded);
    } catch (e) {
      console.log("Cannot getPublicVideos::", e);
      this.setVideoStatus(VideoStatus.undefined);
    }
  }

  @Action
  async getAndSaveVideo(videoId: string) {
    try {
      this.setVideoStatus(VideoStatus.loading);
      const response = await this.publicVideoService.getVideo(videoId);
      console.log("Video::", response.results);
      this.saveVideo(response.results);
      this.dataManager.saveVideo(response.results);
      this.setVideoStatus(VideoStatus.loaded);
    } catch (e) {
      console.log("Cannot get Video ", e);
      this.setVideoStatus(VideoStatus.undefined);
    }
  }

  @Action
  async updateView(videoId: string): Promise<void> {
    try {
      this.setVideoStatus(VideoStatus.loading);
      const response = await this.privateVideosService.updateViews(videoId);
      this.saveVideo(response.results);
      this.setVideoStatus(VideoStatus.loaded);
    } catch (e) {
      console.log("Cannot get Video ", e);
      this.setVideoStatus(VideoStatus.loaded);
    }
  }

  @Action
  async updateVideo(payload: any) {
    try {
      const {
        videoId,
        status,
        userId,
        thumbnailUrl,
        title,
        description,
        category
      } = payload;
      await this.privateVideosService.updateVideo(
        videoId,
        status,
        userId,
        thumbnailUrl,
        title,
        description,
        category
      );
    } catch (e) {
      console.log("Cannot update video::", e);
    }
  }
}

export const VideosModule = getModule(VideoStore);

import store from "@/store";
import {
  getModule,
  Module,
  Action,
  VuexModule,
  Mutation
} from "vuex-module-decorators";
import { InjectValue } from "typescript-ioc";
import { AuthenticationService, DataManager } from "@core/services";
import {
  LoginRequest,
  OAuthLoginRequest
} from "@core/domain/request/login.request";
import { DI, DIKeys } from "@core/modules";
import { RegisterRequest } from "@core/domain/request";
import { VueRouter } from "vue-router/types/router";
import {TokenResponse, UserResponse} from "@core/domain/response/user.response";
import { Routes, Stores } from "@/shared/enums";

export enum AuthenticationStatus {
  UnIdentify,
  Authenticating,
  Authenticated,
  UnAuthenticated
}

export interface AuthenticationState {
  authStatus: AuthenticationStatus;
  errorMessage: string;
  notifyMessage: string;
}

@Module({
  store: store,
  name: Stores.authenticationStore,
  dynamic: true,
  namespaced: true
})
export class AuthenticationStore extends VuexModule {
  //state
  authStatus: AuthenticationState["authStatus"] =
    AuthenticationStatus.UnIdentify;
  errorMessage: AuthenticationState["errorMessage"] = "";
  notifyMessage: AuthenticationState["notifyMessage"] = "";
  public router!: VueRouter;
  @InjectValue(DIKeys.noAuthService)
  private noAuthenticationService!: AuthenticationService;

  @InjectValue(DIKeys.authService)
  private authenticationService!: AuthenticationService;

  @Mutation
  private setAuthStatus(state: AuthenticationStatus): void {
    this.authStatus = state;
  }

  @Mutation
  private setError(error: string): void {
    this.errorMessage = error;
  }

  @Mutation
  private setNotifyMessage(mess: string): void {
    this.notifyMessage = mess;
  }

  @Action
  private async saveSessionAndUserProfile(token: string) {
    const dataManager = DI.get(DataManager);
    dataManager.saveSession(token);
    try {
      const response = await this.authenticationService.getUserLogged();
      if (response.results) {
        dataManager.saveUserProfile(response.results);
      }
    } catch (e) {
      console.log("Cannot save session and user profile::", e);
    }
  }

  @Action
  async register(payload: RegisterRequest) {
    const { channelName, email, password } = payload;
    try {
      const response = await this.noAuthenticationService.register({
        channelName: channelName,
        email: email,
        password: password
      });
      if (response.success) {
        this.setNotifyMessage("You can login with your account registered");
      }
    } catch (e) {
      console.log("AuthenticationStore :: REGISTER :: Errors :", e);
      this.setError(e);
    }
  }

  @Action
  async login(payload: LoginRequest) {
    let response: TokenResponse;
    const { email, password } = payload;
    try {
      this.setAuthStatus(AuthenticationStatus.Authenticating);
      response = await this.noAuthenticationService.login({
        email: email,
        password: password
      });
      await this.saveSessionAndUserProfile(response.results);
      this.setAuthStatus(AuthenticationStatus.Authenticated);
      this.router.replace({ name: Routes.home });
    } catch (e) {
      console.log("AuthenticationStore :: LOGIN :: Errors :", e);
      this.setAuthStatus(AuthenticationStatus.UnIdentify);
      this.setError(e);
    }
  }

  @Action
  async oauthLogin(payload: OAuthLoginRequest) {
    let response: TokenResponse;
    const { provider, token } = payload;
    try {
      this.setAuthStatus(AuthenticationStatus.Authenticating);
      response = await this.noAuthenticationService.oauthLogin({
        provider: provider,
        token: token
      });
      await this.saveSessionAndUserProfile(response.results);
      this.setAuthStatus(AuthenticationStatus.Authenticated);
      this.router.replace({ name: Routes.home });
    } catch (e) {
      console.log("AuthenticationStore :: LOGIN :: Errors :", e);
      this.setAuthStatus(AuthenticationStatus.UnIdentify);
      this.setError(e);
    }
  }

  @Action
  async forgotPassword(email: string): Promise<boolean> {
    try {
      this.setAuthStatus(AuthenticationStatus.Authenticating);
      await this.noAuthenticationService.forgotPassword(email);
      this.setAuthStatus(AuthenticationStatus.UnIdentify);
      return true;
    } catch (e) {
      console.log("AuthenticationStore :: forgotPassword :: Errors :", e);
      this.setError(e);
      this.setAuthStatus(AuthenticationStatus.UnIdentify);
      return false;
    }
  }

  @Action
  async resetPassword(token: string): Promise<boolean> {
    const newPassword = "1234567a";
    try {
      await this.noAuthenticationService.resetPassword(token, newPassword);
      return true;
    } catch (e) {
      console.log("AuthenticationStore :: resetPassword :: Errors :", e);
      this.setError(e);
      return false;
    }
  }

  @Action
  async updatePassword(payload: any) {
    try {
      const { currentPassword, newPassword } = payload;
      await this.authenticationService.updatePassword(
        currentPassword,
        newPassword
      );
    } catch (e) {
      console.log("update password :: error::", e);
    }
  }

  @Action
  async updatePersonalInfo(payload: any) {
    try {
      const { email, channelName } = payload;
      await this.authenticationService.updateDetails(email, channelName);
    } catch (e) {
      console.log("update person info::error:", e);
    }
  }

  @Action
  async signOut() {
    const dataManager = DI.get(DataManager);
    dataManager.clearUserData();
    dataManager.clearVideo();
    dataManager.clearSubscribers();
    this.router.replace({ name: Routes.welcomePage });
  }
}

export const AuthenticationModule = getModule(AuthenticationStore);

import store from "@/store";
import {
  Action,
  getModule,
  Module,
  Mutation,
  VuexModule
} from "vuex-module-decorators";
import { UserInfo } from "@core/domain/models/user_info.model";
import { Inject, InjectValue } from "typescript-ioc";
import { AuthenticationService, DataManager } from "@core/services";
import { DIKeys } from "@core/modules";
import { Stores } from "@/shared/enums";

export interface UserInfoState {
  userInfo: UserInfo;
}

@Module({
  store: store,
  namespaced: true,
  name: Stores.userDataStore,
  dynamic: true
})
export class UserDataStore extends VuexModule {
  user: UserInfoState["userInfo"] = new UserInfo(
    "",
    "",
    "",
    "",
    new Date(),
    new Date(),
    0,
    ""
  );

  @Inject
  dataManager!: DataManager;
  @InjectValue(DIKeys.authService)
  authenticationService!: AuthenticationService;

  get getUserInfo(): UserInfo {
    if (this.user.id === "") return this.dataManager.getUserProfile();
    return this.user;
  }

  @Mutation
  setUserInfo(userInfo: UserInfo) {
    this.user = userInfo;
  }

  @Action
  async getUserLogged() {
    try {
      const response = await this.authenticationService.getUserLogged();
      this.setUserInfo(response.results);
      this.dataManager.saveUserProfile(response.results);
      console.log("get user data logged::", response.results);

    } catch (e) {
      console.log("Cannot get user data logged::", e);
    }
  }
}

export const UserDataModule = getModule(UserDataStore);

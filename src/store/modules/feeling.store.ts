import {
  Action,
  getModule,
  Module,
  Mutation,
  VuexModule
} from "vuex-module-decorators";
import store from "@/store";
import { Stores } from "@/shared/enums";
import { Inject } from "typescript-ioc";
import { FeelingsService } from "@core/services/feelings.service";
import { VideosModule } from "@/store/modules/video.store";
import { StringUtils } from "@/shared/utils/string.utils";

export enum Emotions {
  noEmotion,
  like,
  dislike
}

export interface FeelingState {
  feel: Emotions;
}

@Module({
  store: store,
  name: Stores.feelingStore,
  dynamic: true,
  namespaced: true
})
export class FeelingStore extends VuexModule {
  feel: FeelingState["feel"] = Emotions.noEmotion;
  @Inject
  feelingsService!: FeelingsService;

  get getFeeling() {
    return this.feel;
  }

  @Mutation
  setFeeling(feel: Emotions) {
    this.feel = feel;
  }

  @Mutation
  setEmotion(messageResponse: string, type: string) {
    if (messageResponse == "create") {
      if (type == "like") {
        this.setFeeling(Emotions.like);
      } else if (type == "dislike") {
        this.setFeeling(Emotions.dislike);
      }
    } else if (messageResponse == "update") {
      if (type == "like") {
        this.setFeeling(Emotions.dislike);
      } else if (type == "dislike") {
        this.setFeeling(Emotions.like);
      }
    }
  }

  @Mutation
  resetFeeling() {
    this.feel = Emotions.noEmotion;
  }

  @Action
  async checkFeeling(): Promise<boolean> {
    try {
      const response = await this.feelingsService.checkFeeling(
        VideosModule.getVideo.id
      );
      if (typeof response.results === null) {
        this.setFeeling(Emotions.noEmotion);
        console.log("checkFeeling::no emotion");
      } else {
        if (response.results.type === "like") {
          this.setFeeling(Emotions.like);
          console.log("checkFeeling::like");
        } else if (response.results.type === "dislike") {
          this.setFeeling(Emotions.dislike);
          console.log("checkFeeling::dislike");
        }
      }

      return Promise.resolve(!!response);
    } catch (e) {
      console.log("cannot check feelings", e);
      return Promise.reject(false);
    }
  }

  @Action
  async alterFeeling(payload: any) {
    try {
      const { videoId, type } = payload;
      const response = await this.feelingsService.changeFeeling(videoId, type);
      console.log(response);
      if (response.message === "remove") {
        this.setFeeling(Emotions.noEmotion);
      }
      return this.setEmotion(response.message, type);
    } catch (e) {
      console.log("cannot alter feeling::", e);
    }
  }

  @Action
  async likedVideo() {
    try {
      await this.feelingsService.getLikedVideo();
    } catch (e) {
      console.log("cannot get liked video::", e);
    }
  }
}

export const FeelingModule = getModule(FeelingStore);

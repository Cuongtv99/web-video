import store from "@/store";
import {
  Action,
  getModule,
  Module,
  Mutation,
  VuexModule
} from "vuex-module-decorators";
import { Inject, InjectValue } from "typescript-ioc";

import { UploadFileService } from "@core/services/upload_file.service";
import { DIKeys } from "@core/modules";
import { AuthenticationService, DataManager } from "@core/services";
import { UserDataModule } from "@/store/modules/user_data.store";
import { Stores } from "@/shared/enums";

export enum FileStatus {
  undefine,
  loading,
  loaded
}

export interface FileState {
  fileStatus: FileStatus;
}

@Module({
  store: store,
  name: Stores.fileStore,
  dynamic: true,
  namespaced: true
})
export class FileStore extends VuexModule {
  statusFile: FileState["fileStatus"] = FileStatus.undefine;
  @InjectValue(DIKeys.authService)
  private authenticationService!: AuthenticationService;

  @Inject
  private dataManager!: DataManager;
  @Inject
  private uploadFile!: UploadFileService;

  get status() {
    return this.statusFile;
  }

  @Mutation
  private setStatus(status: FileStatus) {
    this.statusFile = status;
  }

  @Action
  async uploadImage(file: File) {
    try {
      this.setStatus(FileStatus.loading);
      const id = UserDataModule.getUserInfo.id;
      // save firebase && save database
      const url = await this.uploadFile.uploadImage(file, id!);
      // update userInfo state
      const response = await this.authenticationService.updateAvatar(url);
      if (response.success) {
        this.dataManager.saveUserProfile(response.results);
        UserDataModule.setUserInfo(this.dataManager.getUserProfile());
        console.log(this.dataManager.getUserProfile());
      }
      this.setStatus(FileStatus.loaded);
    } catch (e) {
      console.log("ERROR::", e);
      this.setStatus(FileStatus.undefine);
    }
  }

  @Action
  async uploadVideo(file: File) {
    try {
      this.setStatus(FileStatus.loading);

      const id = UserDataModule.getUserInfo.id;
      // save firebase
      console.log("upload::", this.uploadFile);
      const response = await this.uploadFile.uploadVideo(file, id!);
      this.setStatus(FileStatus.loaded);
      return Promise.resolve(response!);
    } catch (e) {
      console.log("ERROR::", e);
      this.setStatus(FileStatus.undefine);
      return Promise.reject(e);
    }
  }

  @Action
  async uploadThumbnail(file: File): Promise<string> {
    try {
      this.setStatus(FileStatus.loading);
      const id = UserDataModule.getUserInfo.id;
      // save firebase
      const response = await this.uploadFile.uploadThumbnail(file, id!);
      this.setStatus(FileStatus.loaded);
      return Promise.resolve(response!);
    } catch (e) {
      console.log("ERROR::", e);
      return Promise.reject(e);
      this.setStatus(FileStatus.undefine);
    }
  }

  @Action
  async deleteThumbnail(payload: any): Promise<boolean> {
    try {
      this.setStatus(FileStatus.loading);

      const { nameFile, userId } = payload;
      const response = await this.uploadFile.deleteThumbnail(nameFile, userId);
      this.setStatus(FileStatus.loaded);

      return Promise.resolve(!!response);
    } catch (e) {
      console.log("cannot delete thumbnail::", e);
      this.setStatus(FileStatus.loaded);

      return Promise.reject(false);
    }
  }

  @Action
  async deleteVideo(payload: any): Promise<boolean> {
    try {
      this.setStatus(FileStatus.loading);

      const { nameFile, userId } = payload;
      const response = await this.uploadFile.deleteVideo(nameFile, userId);
      this.setStatus(FileStatus.loaded);

      return Promise.resolve(!!response);
    } catch (e) {
      console.log("cannot delete video::", e);
      this.setStatus(FileStatus.undefine);

      return Promise.reject(false);
    }
  }

  @Action
  async deleteImage(file: string): Promise<boolean> {
    try {
      const id = UserDataModule.getUserInfo.id;
      const response = await this.uploadFile.deleteImage(file, id!);
      return Promise.resolve(!!response);
    } catch (e) {
      console.log("cannot delete image::", e);
      this.setStatus(FileStatus.undefine);

      return Promise.reject(false);
    }
  }
}

export const FileModule = getModule(FileStore);

import {
  Action,
  getModule,
  Module,
  Mutation,
  VuexModule
} from "vuex-module-decorators";
import store from "@/store";
import { Stores } from "@/shared/enums";
import { History } from "@core/domain/models/history.model";
import { Inject } from "typescript-ioc";
import { HistoriesService } from "@core/services/histories.service";
import { HistoryModel } from "@/shared/models/history.model";
import { Transform } from "@/shared/utils/transform.utils";

export interface HistoryState {
  history: History[];
}

@Module({
  store: store,
  name: Stores.historyStore,
  dynamic: true,
  namespaced: true
})
export class HistoryStore extends VuexModule {
  _listHistories: HistoryState["history"] = [];
  @Inject
  historyService!: HistoriesService;

  get listHistories() {
    let histories: HistoryModel[] = [];
    this._listHistories.map(value => {
      if (value) histories = [...histories, Transform.toHistoryModel(value)];
    });
    console.log("history::", histories);
    return histories;
  }

  @Mutation
  saveHistories(histories: History[]) {
    this._listHistories = [...histories];
  }

  @Action
  async createHistory(payload: any) {
    try {
      const { type, searchText, videoId } = payload;
      await this.historyService.createHistory(type, searchText, videoId);
    } catch (e) {
      console.log("Cannot create hisotry::", e);
    }
  }

  @Action
  async getHistories() {
    try {
      const response = await this.historyService.getHistories();
      console.log(response.results);
      this.saveHistories(response.results);
    } catch (e) {
      console.log("cannot get histories::", e);
    }
  }

  @Action
  async deleteHistory(id: string) {
    try {
      await this.historyService.deleteHistory(id);
    } catch (e) {
      console.log("cannot delete history::", e);
    }
  }

  @Action
  async deleteHistories(type: string) {
    try {
      await this.historyService.deleteHistories(type);
    } catch (e) {
      console.log("cannot delete histories::", e);
    }
  }
}

export const HistoryModule = getModule(HistoryStore);

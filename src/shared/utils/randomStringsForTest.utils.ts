export function randomString() {
  const chars = "abcdefghijklmnopqrstuvwxyz1234567890";
  let strings = "";
  for (let ii = 0; ii < 15; ii++) {
    strings += chars[Math.floor(Math.random() * chars.length)];
  }
  return strings;
}

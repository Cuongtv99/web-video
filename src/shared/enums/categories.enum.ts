export class Type {
  static readonly Movies = [
    "Action",
    "Adventure",
    "Animated",
    "Biography",
    "Cartoons",
    "Comedy",
    "Documentary Film",
    "Drama",
    "Horror Film",
    "Romance",
    "Musical",
    "Romantic Comedy",
    "Science Fiction",
    "Thriller",
    "War Film",
    "Western"
  ];
}

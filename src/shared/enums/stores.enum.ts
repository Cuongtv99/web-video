export enum Stores {
  authenticationStore = "authenticationStore",
  userDataStore = "userDataStore",
  videosStore = "videosStore",
  categoriesStore = "categoriesStore",
  fileStore = "fileStore",
  commentStore = "commentStore",
  feelingStore = "feelingStore",
  replyStore = "replyStore",
  subscriptionStore = "subscriptionStore",
  searchStore = "searchStore",
  historyStore = "historyStore"
}

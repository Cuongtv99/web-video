export class UserModel {
  name!: string;
  photoUrl!: string;
  url!: string;
  subscribers!: number;
  id!: string;
}

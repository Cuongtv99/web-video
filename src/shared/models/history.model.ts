import { VideoModel } from "@/shared/models/video.model";
import { UserModel } from "@/shared/models/user.model";

export class HistoryModel {
  id!: string;
  type!: string;
  searchText?: string;
  videoId?: VideoModel;
  userId!: UserModel;
  createdAt!: string;
  updatedAt!: string;
}

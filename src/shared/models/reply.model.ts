import { UserInfo } from "@core/domain/models/user_info.model";

export class ReplyModel {
  id!: string;
  text!: string;
  userId!: UserInfo;
  commentsId!: string;
  createdAt!: string;
  updatedAt!: string;
}

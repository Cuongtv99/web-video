import { VideoModel } from "@/shared/models/video.model";
import { UserModel } from "@/shared/models/user.model";

export class SearchUserModel {
  photoUrl!: string;
  channelName!: string;
  email!: string;
  createdAt!: string;
  updatedAt!: string;
  subscribers!: number;
  videos?: VideoModel[];
  id!: string;
  link!: string;
}

export class SearchVideoModel {
  createdAt!: string;
  description!: string;
  id!: string;
  status!: string;
  thumbnailUrl!: string;
  title!: string;
  updatedAt!: string;
  url!: string;
  link!: string;
  userId!: UserModel;
  views!: number;
}
